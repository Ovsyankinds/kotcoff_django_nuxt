from rest_framework import serializers
from .model import *


class ProductsWeightSerializer(serializers.ModelSerializer):
  class Meta:
    model = ProductsWeight
    fields = '__all__'


class ProductsPricesWeightSerializer(serializers.ModelSerializer):
  class Meta:
    model = ProductsPrices
    fields = '__all__'


class ProductsSerializer(serializers.ModelSerializer):
  # product_weight = ProductsWeightSerializer(read_only=True, many=True)
  # product_prices = ProductsPricesWeightSerializer(read_only=True)
  product_prices = ProductsPricesWeightSerializer(read_only=True,  many=True)
  class Meta:
    model = Products
    fields = '__all__'