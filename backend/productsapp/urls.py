from rest_framework import routers
from .api import ProductsSet, ProductsWeightSet

router = routers.DefaultRouter()

router.register('api/products', ProductsSet, 'products')
router.register('api/productsWeight', ProductsWeightSet, 'productsWeigth')

urlpatterns = router.urls