from django.contrib import admin
from .model import Products, ProductsWeight, ProductsPrices

class ProductsAdmin(admin.ModelAdmin):
  list_display = ('id', 'product_name', 'product_description', 'product_image', 'product_sort')
  list_display_links = ('id', 'product_name')
  search_fields = ('id', 'product_name')


# class ProductsWeightAdmin(admin.ModelAdmin):
#   list_display = ('id', 'weight')
#   list_display_links = ('id', 'weight')
#   search_fields = ('id', 'weight')


class ProductsPricesWeightAdmin(admin.ModelAdmin):
  list_display = ('product_id', 'product_weight', 'product_price', 'product_weight_checked',)
  list_display_links = ('product_id', 'product_weight', 'product_price',)
  search_fields = ('product_id', 'product_weight', 'product_price',)

admin.site.register(Products, ProductsAdmin)
# admin.site.register(ProductsWeight, ProductsWeightAdmin)
admin.site.register(ProductsPrices, ProductsPricesWeightAdmin)