from django.db import models
from django.utils import timezone
from django.utils.text import slugify


class ProductsWeight(models.Model):
	weight = models.IntegerField(verbose_name='Вес продукта')

	class Meta:
		verbose_name = 'Вес продуктов'
		verbose_name_plural = 'Вес продуктов'


class ProductsPrices(models.Model):
	product_id = models.AutoField(primary_key=True)
	product_weight = models.IntegerField(verbose_name='Вес продукта')
	product_price = models.IntegerField(verbose_name='Цена продукта')
	product_weight_checked = models.BooleanField(verbose_name='Изначально установленный вес')

	class Meta:
		verbose_name = 'Цена продукта'
		verbose_name_plural = 'Цена продукта'

	def __str__(self):
		return str(self.product_price)


class Products(models.Model):
	# PRODUCT_WEIGHT = (
	# 	(200, 200),
	# 	(1000, 1000)
	# )
	
	product_name = models.CharField(max_length=150, verbose_name='Название продукта')
	product_description = models.TextField(max_length=1000, verbose_name='Описание продукта')
	product_image = models.ImageField(upload_to='images/%Y/%m/%d', verbose_name='Картинка для продукта', blank=True)
	product_mobile_image = models.ImageField(upload_to='images/mobile/%Y/%m/%d', verbose_name='Мобильная картинка для продукта' )
	product_sort = models.IntegerField(verbose_name='Номер сортировки', default=1)
	# product_weight = models.IntegerField(verbose_name='Вес продукта', default=200, choices=PRODUCT_WEIGHT)
	product_prices = models.ManyToManyField(ProductsPrices, verbose_name='Цена продукта')
	
	class Meta: 
		verbose_name = 'Продукт'
		verbose_name_plural = 'Продукт'