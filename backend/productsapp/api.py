from .model import *
from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from .serializers import *
from rest_framework import filters

class ProductsSet(viewsets.ModelViewSet):
  queryset = Products.objects.all().order_by('product_sort')
  permissions_classes = [
    permissions.AllowAny
  ]
  serializer_class = ProductsSerializer


class ProductsWeightSet(viewsets.ModelViewSet):
  queryset = ProductsWeight.objects.all()
  permissions_classes = [
    permissions.AllowAny
  ]
  serializer_class = ProductsWeightSerializer