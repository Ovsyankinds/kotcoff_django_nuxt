from django.contrib import admin
from .model import Slider

class SliderAdmin(admin.ModelAdmin):
  list_display = ('id', 'title', 'text', 'image', 'sort')
  list_display_links = ('id', 'title')
  search_fields = ('id', 'title')

admin.site.register(Slider, SliderAdmin)