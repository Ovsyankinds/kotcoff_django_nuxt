from rest_framework import serializers
from .model import *

class SliderSerializer(serializers.ModelSerializer):

  class Meta:
    model = Slider
    fields = '__all__'