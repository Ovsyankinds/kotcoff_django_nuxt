from django.db import models
from django.utils import timezone
from django.utils.text import slugify


class Slider(models.Model):
	title = models.CharField(max_length=150, verbose_name='Заголовок')
	text = models.TextField(max_length=1000, verbose_name='Текст')
	image = models.ImageField(upload_to='images/%Y/%m/%d', verbose_name='Картинка для слайдера', blank=True)
	mobile_image = models.ImageField(upload_to='images/mobile/%Y/%m/%d', verbose_name='Мобильная картинка для слайдера' )
	sort = models.IntegerField(verbose_name='Номер сортировки', default=1)
	
	class Meta:
		verbose_name = 'Слайдер'
		verbose_name_plural = 'Слайдер'