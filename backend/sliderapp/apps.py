from django.apps import AppConfig


class SliderappConfig(AppConfig):
    name = 'sliderapp'