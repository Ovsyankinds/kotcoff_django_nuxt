from rest_framework import routers
from .api import SliderSet

router = routers.DefaultRouter()

router.register('api/slider', SliderSet, 'slider')

urlpatterns = router.urls