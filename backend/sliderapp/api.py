from .model import *
from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from .serializers import *
from rest_framework import filters

class SliderSet(viewsets.ModelViewSet):
  queryset = Slider.objects.all().order_by('sort')
  permissions_classes = [
    permissions.AllowAny
  ]
  serializer_class = SliderSerializer