import productCart from './product-cart'
import products from './products'

export default {
  actions: {
    nuxtServerInit({ dispatch }, context) {
      return Promise.all([
      ])
    }
  },
  modules: {
    productCart: productCart,
    products: products
  }
}