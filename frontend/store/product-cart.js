export default {
  namespaced: true,
  state() {
    return {
      productCart: [],
      totalProduct: 0,
      flag: false
    }
  },
  getters: {
  },
  mutations: {
    setProductCart(state, product) {
      product.product_prices = product.product_prices.filter( item => item.product_weight_checked === true)
      state.totalProduct += product.count

      if (state.productCart.length > 0) {
        let id = state.productCart.findIndex(item => item.id === product.id && item.product_prices[0].product_weight === product.product_prices[0].product_weight)
        if(state.productCart.find(item => item.id === product.id && item.product_prices[0].product_weight === product.product_prices[0].product_weight)) {
          console.log('else', id)
          state.productCart[id].count += product.count
        } else state.productCart.push(product)
      } else {
        state.productCart.push(product)
      }
    },
    deleteProductInCart(state, product) {
      state.totalProduct -= product.count
      let id = state.productCart.findIndex(item => item.id === product.id && item.product_prices[0].product_weight === product.product_prices[0].product_weight)
      state.productCart.splice(id, 1)
    }
  },
  actions: {
  }
}