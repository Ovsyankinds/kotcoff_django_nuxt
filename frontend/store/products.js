export default {
  namespaced: true,
  state() {
    return {
      // productsArray: [
      //   {
      //     productId: 0,
      //     productName: "CUAPERO COFFEE молотый 1",
      //     productDescription: "По-настоящему природный кофе из созревших плодов кофейного дерева, богатых эфирными маслами и смолами 1",
      //     productWeightAndPrice: { "productWeightId": 0, "productPrice": 500, "productWeight": 200, checked: true },
      //     // productWeightAndPrice: [
      //     //   { "productWeightId": 0, "productPrice": 500, "productWeight": 200, checked: true },
      //     //   { "productWeightId": 1, "productPrice": 1200, "productWeight": 1000, checked: false },
      //     // ],
      //     count: 1
      //   },
      //   // {
      //   //   productId: 1,
      //   //   productName: "CUAPERO COFFEE молотый 2",
      //   //   productDescription: "По-настоящему природный кофе из созревших плодов кофейного дерева, богатых эфирными маслами и смолами 2",
      //   //   productWeightAndPrice: [
      //   //     { "productWeightId": 0, "productPrice": 500, "productWeight": 200, checked: true },
      //   //     { "productWeightId": 1, "productPrice": 1200, "productWeight": 1000, checked: false },
      //   //   ],
      //   //   count: 1
      //   // },
      //   // {
      //   //   productId: 2,
      //   //   productName: "CUAPERO COFFEE молотый 3",
      //   //   productDescription: "По-настоящему природный кофе из созревших плодов кофейного дерева, богатых эфирными маслами и смолами 3",
      //   //   productWeightAndPrice: [
      //   //     { "productWeightId": 0, "productPrice": 500, "productWeight": 200, checked: true },
      //   //     { "productWeightId": 1, "productPrice": 1200, "productWeight": 1000, checked: false },
      //   //   ],
      //   //   count: 1
      //   // },
      //   // {
      //   //   productId: 3,
      //   //   productName: "CUAPERO COFFEE молотый 4",
      //   //   productDescription: "По-настоящему природный кофе из созревших плодов кофейного дерева, богатых эфирными маслами и смолами 4",
      //   //   productWeightAndPrice: [
      //   //     { "productWeightId": 0, "productPrice": 500, "productWeight": 200, checked: true },
      //   //     { "productWeightId": 1, "productPrice": 1200, "productWeight": 1000, checked: false },
      //   //   ],
      //   //   count: 1
      //   // },
      //   // {
      //   //   productId: 4,
      //   //   productName: "CUAPERO COFFEE молотый 5",
      //   //   productDescription: "По-настоящему природный кофе из созревших плодов кофейного дерева, богатых эфирными маслами и смолами 5",
      //   //   productWeightAndPrice: [
      //   //     { "productWeightId": 0, "productPrice": 500, "productWeight": 200, checked: true },
      //   //     { "productWeightId": 1, "productPrice": 1200, "productWeight": 1000, checked: false },
      //   //   ],
      //   //   count: 1
      //   // },
      // ],
      backendProducts: null
    }
  },
  getters: {
    getProducts(state) {
      return state.backendProducts
    }
  },
  mutations: {
    setProducts(state, value) {
      state.backendProducts = value
    },
    changeCheckedWeight(state, value) {
      // value = {productId, productPricesId}

      let filterProduct = state.backendProducts.filter(item => item.id === value.productId)[0]
      let id = state.backendProducts.findIndex(item => item.id === value.productId)
      if(filterProduct.product_prices.find(item => item.product_id === value.productPricesId)) {
        // console.log('changeCheckedWeight', id)
        state.backendProducts[id].product_prices.forEach( item => {
          if(item.product_weight_checked === true) {
            item.product_weight_checked = false
          }else {
            item.product_weight_checked = true
          }
        })
      }
      console.log('changeCheckedWeight', state.backendProducts)
    },
    productCountChange(state, obj) {
      // console.log('productCountChange', obj)
      // console.log('productCountChange1', state.backendProducts[obj.id])
      if(obj.type === 'down') state.backendProducts[obj.id].count = state.backendProducts[obj.id].count - 1
      else state.backendProducts[obj.id].count = state.backendProducts[obj.id].count + 1
    },
  }
}