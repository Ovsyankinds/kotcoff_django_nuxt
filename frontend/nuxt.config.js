require('dotenv').config()
export default {
  head: {
    title: 'Групповые закупки кофе',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, maximum-scale=1.0' },
      { name: 'yandex-verification', content: '47d1e7def8ff4333' }
    ],
    link: [{ rel: "icon", type: "image/png", href: process.env.BASE_FILE + '/img/favicon.svg' || 'https://api.mos-buro.ru/uploads/files/img/favicon.svg' }],
    script: [
      { src: "//yastatic.net/pcode/adfox/loader.js" },
    ]
  },
  modules: ["@nuxtjs/axios", "@nuxtjs/proxy", "@nuxtjs/router", ['nuxt-sass-resources-loader', 'sass/*.sass'], '@nuxtjs/style-resources'],
  buildModules: [],
  css: [
    "@/sass/main.sass",
    "@/sass/fonts.sass",
    "@/sass/reset.sass",
  ],
  styleResources: {
    sass: [
      '@/sass/vars.sass'
    ]
  },
  axios: {
    // baseURL: process.env.BASE_URL || 'http://api.kotcoff.loc/api',
    // browserBaseURL: process.env.BROWSER_BASE_URL || 'http://api.kotcoff.loc/api'
    // baseURL: 'http://backend:8000/api',
    baseURL: 'http://localhost:8000/api',
    browserBaseURL: 'http://localhost:8000/api',
    // baseURL: 'http://api.kotcoff.loc:8000/api',
    // browserBaseURL: 'http://api.kotcoff.loc:8000/api',
    // proxy: true
  },
  router: {
    middleware: []
  },
	build: {
		vendor: ['vue-awesome-swiper'],
		extend (config, ctx) {
		}
	},
  plugins: [
    { src: '~/plugins/swiper-slider.js', ssr: false },
  ],
  server: {
    host: '0.0.0.0',
    port: '3000'
  }
}
