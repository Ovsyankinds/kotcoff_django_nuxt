import Vue from 'vue'
import Router from 'vue-router'
import { isMobileVersion } from './common/useragent';
Vue.use(Router)

export function createRouter(app) {
  let req = app ? app.req : null;
  let router = new Router({
    mode: 'history',
    routes: [],
  })

  router.beforeEach(function (to, from, next) {
    if (typeof (window) != 'undefined') {
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      })
    }
    next()
  })

  router.addRoutes([
    {
      path: '/',
      component: () =>
        isMobileVersion(req)
          ? import('./modules/index/pages/mobile').then(cmp => cmp.default || cmp)
          : import('./modules/index/pages').then(cmp => cmp.default || cmp),
      name: 'index',
    },
    {
      path: '/checkout',
      component: () =>
        isMobileVersion(req)
          ? import('./modules/checkout/pages/mobile').then(cmp => cmp.default || cmp)
          : import('./modules/checkout/pages').then(cmp => cmp.default || cmp),
      name: 'checkout',
    },
    {
      path: '/research',
      component: () =>
        isMobileVersion(req)
          ? import('./modules/research/pages').then(cmp => cmp.default || cmp)
          : import('./modules/research/pages').then(cmp => cmp.default || cmp),
      name: 'research',
    },
  ])

  return router;
}
