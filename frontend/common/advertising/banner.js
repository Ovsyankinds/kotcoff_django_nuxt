export default {
  data() {
    return {
      item: {},
      show: false,
    }
  },
  props: {
    place: {
      type: String,
      default: 'Внутренние страницы'
    }
  },
  computed: {
    hashId() {
      return [...Array(32)].map(i=>(~~(Math.random()*36)).toString(36)).join('')
    }
  },
  async mounted() {
    if (typeof(window) == 'undefined') return
    if (typeof(window.Ya) == 'undefined' || typeof(window.Ya.adfoxCode) == 'undefined' || typeof(window.Ya.adfoxCode.create) == 'undefined') {
      console.log('Рекламная сеть adfox не подключена')
      return
    }

    let code = {
      adfoxOwnerId: 254083,
      adfoxContainerId: 'adfox_159283680157375980',
      adfoxParams: { p1: 'cowcf', p2: 'hevn' }
    }
    this.item = code
    this.show = true
    // костыль, чтобы дождаться создания контейнера с нужным id
    setTimeout(() => {
      window.Ya.adfoxCode.create({
        ownerId: code.adfoxOwnerId,
        containerId: code.adfoxContainerId + this.hashId,
        params: code.adfoxParams,
        onStub: () => {
          this.show = false
          console.log(`adFox: onStub id = ${code.adfoxContainerId}`)
        },
        onError: () => {
          this.show = false
          console.log(`adFox: onError id = ${code.adfoxContainerId}`)
        },
      })
    }, 500)
  }
}