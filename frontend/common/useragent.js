export {isDesktop, isMobile, isMobileVersion}

function userAgent(req) {
  return process && process.server ? req.headers['user-agent'] : navigator.userAgent
}

function isMobile(req) {
  return /mobile/i.test(userAgent(req)) || /ios/i.test(userAgent(req)) || /android/i.test(userAgent(req))
}

function isDesktop(req) {
  return !isMobile(req)
}

function isMobileVersion(req) {
  return isMobile(req)
}
